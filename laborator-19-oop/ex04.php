<?php

Class MyNewClass {
	public $p1;
	public $p2;
	// method called automatically when the object is created
	public function __construct($param1,$param2){
		$this->p1 = $param1;
		$this->p2 = $param2;
	}
}

$object = new MyNewClass("Audi","A4");
