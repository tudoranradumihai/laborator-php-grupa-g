<?php

// define function
function welcomeMessage($name){
	// body of my function
	return "Welcome ".$name."<br>";
}
// call function
//echo welcomeMessage("John Doe");

Class Users {
	public $email;
	public function welcomeMessage($name){
		return $name."<br>";
	}
	public function setEmail($email){
		$this->email = $email;
	}
	public function getEmail(){
		return $this->email;
	}
}
$user = new Users();
$user->email = "...@...";
echo $user->email."<br>"; // show property
//echo $user->welcomeMessage("John Doe"); // show method

$user->setEmail("john.doe@email.com");
echo $user->getEmail();


Class Products {

	public $name;
	public $description;
	public $price;


	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}
	public function getDescription(){
		return $this->description;
	}

	public function setDescription($description){
		$this->description = $description;
	}

	public function getPrice(){
		return $this->price;
	}

	public function setPrice($price){
		$this->price = $price;
	}

}

$product = new Products();
$product->setName("Product Name");
$product->setDescription("Product Description");
$product->setPrice(100);


