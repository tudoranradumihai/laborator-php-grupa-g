CREATE TABLE IF NOT EXISTS products (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255),
	description TEXT,
	price FLOAT(11.2),
	stock INT(11),
	/* cheie straina catre categorii */
	category INT(11),
	createddate DATETIME DEFAULT CURRENT_TIMESTAMP,
	/* facem legatura cu tabelul de categorii */
	FOREIGN KEY (category) REFERENCES categories(id)
);