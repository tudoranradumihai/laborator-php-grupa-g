<?php

include("setup.php");

/*
Incercam sa ne conectam la MySQL cu user,pass si baza de date
*/
$connection = @mysqli_connect(HOSTNAME,USERNAME,PASSWORD,DATABASE);
/*
Daca conexiunea a esuat incercam sa ne conectam cu user si pass crezand ca problema vine de la inexistenta bazei de date
*/
if(!$connection){
	$connection = @mysqli_connect($hostname,$username,$password);
	if($connection){
		/*
		creem baza de date
		*/
		$query = "CREATE DATABASE `$database`;";
		$result = mysqli_query($connection,$query);
		if($result){
			echo "Database '$database' created.<br>";
		}
		/*
		atasam noua baza de date creata la conexiunea existenta
		*/
		mysqli_select_db($database);
	} else {
		echo "ERROR: ".mysqli_connect_error();
	}
}

if($connection){
	$files = scandir("SQL");
	$files = array_diff($files,array(".",".."));
	foreach($files as $file){
		$path = "SQL/$file";
		$query = file_get_contents($path);
		$result = mysqli_query($connection,$query);
		if($result){
			echo "Table '$file' created / already exists.<br>";
		}
	}
	mysqli_close($connection);
}