<!-- AFISARE ERORI PRIN SESSION -->
<?php
include "setup.php";
$connection = mysqli_connect(HOSTNAME,USERNAME,PASSWORD,DATABASE);
$query = "SELECT * FROM categories";
$result = mysqli_query($connection,$query);
?>
<form method="POST" action="create-product.php">
	Name:
	<input type="text" name="name"><br>
	Description:
	<textarea name="description"></textarea><br>
	Price:
	<input type="text" name="price"><br>
	Stock:
	<input type="text" name="stock"><br>
	Category:
	<select name="category">
		<option value="">...</option>
		<?php
			while($line = mysqli_fetch_assoc($result)){
				echo '<option value="'.$line["id"].'">'.$line["name"].'</option>';
			}
		?>
	</select><br>
	<input type="submit">
</form>