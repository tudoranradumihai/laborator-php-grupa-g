<a href="new-product.php">New Product</a> | <a href="new-category.php">New
Category</a> <br> 
<a href="install.php">Install</a>

<?php
include("setup.php");
$connection = mysqli_connect(HOSTNAME,USERNAME,PASSWORD,DATABASE);
if(!$connection){
	die();
}
$query = "SELECT 
		products.id AS 'id', 
	    products.name AS 'product', 
	    products.price AS 'price',
	    products.stock AS 'stock',
	    categories.name AS 'category'
	FROM products
	LEFT JOIN categories 
		ON products.category = categories.id";
$result = mysqli_query($connection,$query);
while($line = mysqli_fetch_assoc($result)){
?>
<div>
	<h1><?php echo $line["product"];?></h1>
	<b>Category: <?php echo $line["category"];?></b><br>
</div>
<?php
}