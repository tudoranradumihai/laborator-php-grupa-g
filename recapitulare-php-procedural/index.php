<?php
session_start();

function checkField($field,$value){
	if(array_key_exists("fields", $_SESSION)){
		if(array_key_exists($field, $_SESSION["fields"])){
			if($value==$_SESSION["fields"][$field]){
				echo "checked";
			}
		}
	}
}

function checkMultipleField($field,$value){
	if(array_key_exists("fields", $_SESSION)){
		if(array_key_exists($field, $_SESSION["fields"])){
			if(in_array($value,$_SESSION["fields"][$field])){
				echo "checked";
			}
		}
	}

}

?>


<form method="POST" action="create.php">
<input name="gender" type="radio" value="1" <?php checkField("gender","1") ?>> M
<input name="gender" type="radio" value="2" <?php checkField("gender","2") ?>> F<br>
<input name="preferences[]" type="checkbox" value="1" <?php checkMultipleField("preferences","1") ?>> A
<input name="preferences[]" type="checkbox" value="2" <?php checkMultipleField("preferences","2") ?>> B<br>
<input type="submit">
</form>