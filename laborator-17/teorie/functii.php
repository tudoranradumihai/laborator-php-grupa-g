<?php

/* MySQL */

// connects to database
$connection = mysqli_connect(/* 4 params */);

/* can be CREATE TABLE, INSERT, SELECT, ... */
$query = " ... ";

/* FOR create table, insert, ... $result can be true/false  */
/* FOR select ... $result will be a mysqli resource */
$result = mysqli_query($connection,$query);

/* IF $result is mysqli resource you need to use a while loop and a mysqli_fetch_assoc call  */

while ($line = mysqli_fetch_assoc($result)){
	/* $line will be one line from the table you selected */
}


// closes database connection
mysqli_close($connection);