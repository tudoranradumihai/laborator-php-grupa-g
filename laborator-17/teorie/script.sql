CREATE TABLE nume_tabel (
	col1 /*...*/ ,
	col2 /*...*/
);

INSERT INTO nume_tabel 
	(col1,col2)
	VALUES 
	("valoarea1","valoare2");

SELECT * FROM nume_tabel;