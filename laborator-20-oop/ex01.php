<?php

Class Class1 {
	public function __construct(){
		echo "Step: 02 - CONSTRUCT<br>";
	}
	public function m1(){
		echo "Step: 03 - M1<br>";
	}
	public function __destruct(){
		echo "Step: 05 - DESTRUCT<br>";
	}
}

echo "Step: 01 - FIRST LINE<br>";
$object = new Class1();
$object->m1();
unset($object);
echo "Step: 04 - LAST LINE<br>";