<?php
/*
Clasa MAMA Animal
proprietati 

greutate
inaltime
coloana

Clasa COPIL Nevertebrat
coloana=false

Clasa COPIL Vertebrat
coloana=true
*/
Class Animal {
	public $greutate;
	public $inaltime;
	public $coloana;
}
Class Nevertebrat extends Animal {
	public function __construct(){
		$this->coloana = false;
	}
}