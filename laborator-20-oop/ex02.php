<?php

Class C1 {
	public $p1;
	public function m1(){

	}
}

Class C2 extends C1 {
	public $p2;
}
$o1 = new C1();
var_dump($o1);
$o2 = new C2();
var_dump($o2);