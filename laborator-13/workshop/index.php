<?php

// http://php.net/manual/ro/function.file-exists.php
if(!file_exists("files")){
	// http://php.net/manual/en/function.mkdir.php
	mkdir("files");
}

$files = scandir("files");
/*
Prin utilizarea functiei array_diff, $files o sa contina toate elementele din $files cu exceptia celor doua elemente continute in al 2-lea parametru a functiei array_diff; pe scurt, o sa stearga din $files elementele "." si ".."
*/
$files = array_diff($files, array(".",".."));
/*
Resetam cheile array-ului; nu este necesar, dar e best practice
*/
$files = array_values($files);
?>
<a href="add.php">Add File</a>
<h1>My Files</h1>
<!-- V1 cu cod HTML direct -->
<?php
foreach($files as $file){
?>
	<a href="files/<?php echo $file ?>"><?php echo $file ?></a>
	<a href="edit.php?file=<?php echo $file ?>">Edit</a>
	<br>
<?php
}
?>

<?php die(); // am pus die ca sa nu se ruleze si codul de mai jos ?> 
<!-- V2 cu HTML injectat din PHP cu ghilimele duble -->
<?php
foreach($files as $file){
	echo "<a href=\"files/".$file."\">".$file."</a><br>";
}
?>

<!-- V3 cu HTML injectat din PHP cu ghilimele simple -->
<?php
foreach($files as $file){
	echo '<a href="files/'.$file.'">'.$file.'</a><br>';
}
?>

<!-- V4 cu HTML injectat din PHP si cu variabile introduse direct in corpul string-ului -->
<?php
foreach($files as $file){
	echo "<a href=\"files/$file\">$file</a><br>";
}
?>