<?php
if($_SERVER["REQUEST_METHOD"]=="POST"){
	/*
	construim calea fisierul de tip folder/numefisier.txt
	*/
	$path = "files/".$_POST["name"].".txt";
	/* 
	deschidem fisierul in modul de scriere
	*/
	$file = fopen($path,"w");
	/*
	preluam continutul din POST
	*/
	$content = $_POST["content"];
	/* 
	adaugam continutul in fisier
	*/
	fputs($file,$content);
	/*
	inchidem fisierul
	*/
	fclose($file);
	header("Location: index.php");

} else {
	header("Location: add.php");
}