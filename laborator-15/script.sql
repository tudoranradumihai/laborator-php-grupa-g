/* 
Create a new database called 'mynewdatabase'
*/
CREATE DATABASE mynewdatabase;

/*
Create a new table called 'myfirsttable'
*/
CREATE TABLE myfirsttable(
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	title VARCHAR(255) NOT NULL,
	description TEXT,
	birthdate DATE,
	lastlogin TIMESTAMP,
	price FLOAT(11.2),

	anotherfield1 INT(11) DEFAULT 0,
	anotherfield2 VARCHAR(255) DEFAULT 'NU NU NU',
	anotherfield3 DATE DEFAULT '2018-01-01',
	anotherfield4 DATE DEFAULT CURRENT_TIMESTAMP, /* iti da data de azi */
	anotherfield5 DATETIME DEFAULT CURRENT_TIMESTAMP, /* iti da data si ora de acum */
);

/*
Create a table if doesn't exist
*/
CREATE TABLE IF NOT EXISTS myfirsttable(
);

/* 
Delete a table
*/
DROP TABLE myfirsttable;

/*
Delete a table if it exists
*/
DROP TABLE IF EXISTS myfirsttable;

/*
Insert data into table
*/
INSERT INTO myfirsttable (title,description) VALUES ('VAL1','VAL2');

INSERT INTO myfirsttable (title,description) VALUES ('VAL1','VAL2'),('VAL3','VAL4');

/*
select stuff
*/
SELECT * FROM myfirsttable;
/*
select all titles and descriptions
*/
SELECT title,description FROM myfirsttable;
/*
select element with id=1
*/
SELECT * FROM myfirsttable WHERE id=1;
/*
select elements who have the bithday on 4 may 1988
*/
SELECT * FROM myfirsttable WHERE birthdate='1988-05-04';
/*
select elements which contain the word "smth" in their title
*/
SELECT * FROM myfirsttable WHERE title LIKE '%smth%';
/*
select elements ordered alphabetically by title 
*/
SELECT * from myfirsttable ORDER BY title ASC;
/*
select first 10 elements that have letter A in their title
*/
SELECT * FROM myfirsttable WHERE title LIKE '%a%' LIMIT 10; 