<?php

$a = true;	//boolean
$b = 1;		//integer
$c = 1.5;	//float
$d = "";	//string

$d = "string";
//echo $d;	//afisare string
//echo $d[4];	//afisare n

$lista = [];
$lista[0] = "ceva";
$lista[1] = 123;

var_dump($lista);

$e = array();
$e[0] = "ceva";

$f = array(1,2,3,4,5);

$user = array();
$user["name"] = "Radu";
$user["papuc"] = 44;
$user["par"] = "nu prea";

echo $user["par"];