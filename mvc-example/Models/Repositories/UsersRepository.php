<?php

Class UsersRepository {

	private $database;

	public function __construct(){
		$this->database = new Database();
	}

	public function findAll(){
		$query = "SELECT * FROM users;";
		$result = $this->database->query($query);
		$array = [];
		while($line = mysqli_fetch_assoc($result)){
			$object = new Users();
			$object->id = $line["id"];
			$object->name = $line["name"];
			$object->email = $line["email"];
			array_push($array,$object);
		}
		return $array;
	}

}
