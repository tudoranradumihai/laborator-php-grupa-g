<?php

Class View {

	private $variables;

	public function assign($key,$value){
		$this->variables[$key] = $value;
	}

	public function render(){
		$backtrace = debug_backtrace();

		$path = "Views/".str_replace("Controller","",$backtrace[1]["class"])."/".str_replace("Action","",$backtrace[1]["function"]).".php";
		if(file_exists($path)){
			foreach($this->variables as $key => $value){
				$$key = $value;
			}
			require $path;
		}
	}


}