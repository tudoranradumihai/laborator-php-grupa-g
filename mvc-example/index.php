<?php
// se recomanda sa punem functia autoload intr-un fisier separat pe care sa il includem in index.php
function __autoload($class){
	$folders = ["Controllers","Models","Models/Repositories","Helpers","Views"];
	foreach ($folders as $folder){
		$path = "$folder/$class.php";
		if(file_exists($path)){
			require $path;
		}
	}
}
?>
<a href="index.php">Homepage</a>
<a href="index.php?C=Users&A=new">New User</a>
<a href="index.php?C=Users&A=list">List Users</a>
<hr>
<?php 
if(array_key_exists("C", $_GET)){
	$controller = $_GET["C"]."Controller";
	if(class_exists($controller)){
		$object = new $controller();
		if(array_key_exists("A", $_GET)){
			$action = $_GET["A"]."Action";
			if(method_exists($object, $action)){
				$object->$action();
			} else {
				echo "Method '$controller::$action' not found.<br>";
			}
		}
	} else {
		echo "Class '$controller' not found.<br>";
	}
}