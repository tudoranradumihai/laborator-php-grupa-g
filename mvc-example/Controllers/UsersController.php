<?php

Class UsersController {

	private $usersRepository;
	private $view;

	public function __construct(){
		$this->usersRepository = new UsersRepository();
		$this->view = new View();
	}

	public function newAction(){
		include "Views/Users/new.php";
	}

	public function createAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$validation = true;

			/*
			tema pentru alexandra validari
			*/
			
			if(array_key_exists("image", $_FILES)){
				if($_FILES["image"]["error"]=="0"){
					if(!file_exists("Resources")){
						mkdir("Resources");
					}
					if(!file_exists("Resources/Images")){
						mkdir("Resources/Images");
					}
					$extension = explode(".",$_FILES["image"]["name"]);
					$extension = $extension[count($extension)-1];
					$path = "users-".date("Ymd-His").".".$extension;
					var_dump($path);
					if(!move_uploaded_file($_FILES["image"]["tmp_name"], "Resources/Images/".$path)){
						$validation = false;
					} else {
						
					}
				}
			} else {
				$validation = false;
			}

			if($validation){
				$object = new Users();
				$object->name = $_POST["name"];
				$object->email = $_POST["email"];
				$object->image = $path;
			}
			//$this->usersRepository->insert($object);
		}
	}

	public function listAction(){
		$users = $this->usersRepository->findAll();
		//require "Views/Users/list.php";
		$this->view->assign("utilizatori",$users);
		$this->view->render();
	}

}