<?php

function initialise(){

	if(array_key_exists("C", $_GET)){
		$controller = ucfirst($_GET["C"])."Controller";
	} else {
		$controller = "ProductsController";
	}

	if(array_key_exists("A", $_GET)){
		$action = lcfirst($_GET["A"])."Action";
	} else {
		$action = "promotionsAction";
	}

	if(class_exists($controller)){
		$object = new $controller();
		if(method_exists($object, $action)){
			echo "<!-- $controller::$action -->";
			$object->$action();
		} else {
			echo "<b>ERROR:</b> Method '$controller::$action' does not exist.<br>";
		}
	} else {
		echo "<b>ERROR:</b> Class '$controller' does not exist.<br>";
	}

}