<?php

function __autoload($class){
	$folders = ["Controllers","Models","Models/Repositories","Helpers"];
	foreach($folders as $folder){
		$path = "$folder/$class.php";
		if(file_exists($path)){
			require $path;
		}
	}
}