CREATE TABLE products (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,

	deleted INT(11) DEFAULT 0,

	name VARCHAR(255),
	description TEXT,
	price FLOAT(11.2),
	stock INT(11),

	category INT(11),

	image VARCHAR(255),

	created DATETIME DEFAULT CURRENT_TIMESTAMP,
	updated DATETIME ON UPDATE CURRENT_TIMESTAMP,

	FOREIGN KEY (category) REFERENCES categories(id)
);