<?php

Class CategoriesController extends Controller {

	private $categoriesRepository;

	public function __construct(){
		$this->categoriesRepository = new CategoriesRepository();
	}

	public function defaultAction(){

	}

	public function newAction(){
		if(array_key_exists("administrator", $_COOKIE)){
			require "Views/Categories/new.php";
		} else {
			header("Location: index.php?C=Users&A=login");
		}
	}

	public function createAction(){
		if(array_key_exists("administrator", $_COOKIE)){
			if($_SERVER["REQUEST_METHOD"]=="POST"){
				$validation = true;

				if($validation){
					$category = new Categories();
					$category->name = $_POST["name"];
					$category->description = $_POST["description"];
					$status = $this->categoriesRepository->insert($category);
					require "Views/Categories/create.php";
				} 
			} else {
				header("Location: index.php?C=Categories&A=new");
			}
		} else {
			header("Location: index.php?C=Users&A=login");
		}
	}

	public function listAction(){
		if(array_key_exists("administrator", $_COOKIE)){
			$categories = $this->categoriesRepository->findAll();
			require "Views/Categories/list.php";
		} else {
			header("Location: index.php?C=Users&A=login");
		}
	}

}