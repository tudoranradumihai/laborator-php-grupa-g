<?php

Class CartController extends Controller {

	public function defaultAction(){

	}

	public function addToCartAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			var_dump($_POST);
			if(array_key_exists("cart", $_SESSION)){
				if(array_key_exists(intval($_POST["product"]), $_SESSION["cart"])){
					$_SESSION["cart"][intval($_POST["product"])] += intval($_POST["quantity"]);
				} else {
					$_SESSION["cart"][intval($_POST["product"])] = intval($_POST["quantity"]);
				}
			} else {
				$_SESSION["cart"] = [];
				$_SESSION["cart"][intval($_POST["product"])] = intval($_POST["quantity"]);
			}
		}
		header("Location: index.php?C=Cart&A=list");
	}

	public function updateAction(){
		if(array_key_exists("cart", $_SESSION)){
			if(array_key_exists(intval($_GET["ID"]), $_SESSION["cart"])){
				if($_GET["QUANTITY"]=="0"){
					unset($_SESSION["cart"][$_GET["ID"]]);
				} else {
				$_SESSION["cart"][intval($_GET["ID"])] = intval($_GET["QUANTITY"]);
				}
			}
		}
		header("Location: index.php?C=Cart&A=list");
	}

	public function listAction(){
		if(array_key_exists("cart", $_SESSION)){
			$ids = array_keys($_SESSION["cart"]);
			$productsRepository = new ProductsRepository();
			$products = $productsRepository->findByIds($ids);
		}
		require "Views/Cart/list.php";
	}

}