<?php


Class DefaultController extends Controller {

	public function defaultAction(){
		require "Views/Default/default.php";
	}

	public function deniedAction(){
		require "Views/Default/denied.php";
	}

}