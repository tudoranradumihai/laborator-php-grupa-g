<?php

Class ProductsController extends Controller {

	private $productsRepository;

	public function __construct(){
		$this->productsRepository = new ProductsRepository();
	}

	public function defaultAction(){

	}

	public function newAction(){
		if(array_key_exists("administrator", $_COOKIE)){
			$categoriesRepository = new CategoriesRepository();
			$categories = $categoriesRepository->findAll();
			require "Views/Products/new.php";
		} else {
			header("Location: index.php?C=Default&A=denied");
		}
	}

	public function createAction(){
		if(array_key_exists("administrator", $_COOKIE)){
			if($_SERVER["REQUEST_METHOD"]=="POST"){
				$validation = true;
				$uploadStatus = false;
				if(array_key_exists("image", $_FILES)){
					if($_FILES["image"]["error"]==0){
						if(!file_exists("Resources")){
							mkdir("Resources");
						}
						if(!file_exists("Resources/Images")){
							mkdir("Resources/Images");
						}
						$path = "Resources/Images/products-".rand(10000,99999)."-".date("Ymd-His").substr($_FILES["image"]["name"],stripos($_FILES["image"]["name"],"."));
						$uploadStatus = move_uploaded_file($_FILES["image"]["tmp_name"], $path);
						if(!$uploadStatus){
							$validation = false;
						}
					} else {
						$validation = false;
						// to add error message in the error array
					}
				}

				if($validation){
					$product = new Products();
					$product->name = $_POST["name"];
					$product->description = $_POST["description"];
					$product->price = $_POST["price"];
					$product->stock = $_POST["stock"];
					$product->category = $_POST["category"];
					if($uploadStatus){
						$product->image = $path;
					}
					$status = $this->productsRepository->insert($product);
					require "Views/Products/create.php";
				} 
			} else {
				header("Location: index.php?C=Products&A=new");
			}
		} else {
			header("Location: index.php?C=Default&A=denied");
		}
	}

	public function listAction(){
		if(array_key_exists("administrator", $_COOKIE)){
			$products = $this->productsRepository->findAll();
			require "Views/Products/list.php";
		} else {
			header("Location: index.php?C=Users&A=login");
		}
	}

	public function showAction(){
		if(array_key_exists("ID", $_GET)){
			$product = $this->productsRepository->findById($_GET["ID"]);
			require "Views/Products/show.php";
		} else {
			header("Location: index.php?C=Products&A=list");
		}
	}

	public function promotionsAction(){
		$categoriesRepository = new CategoriesRepository();
		$categories = $categoriesRepository->findAll();
		$products = $this->productsRepository->findAll();
		require "Views/Products/promotions.php";
	}

	public function searchByCategoryAction(){
		if(array_key_exists("ID", $_GET)){
			$categoriesRepository = new CategoriesRepository();
			$categories = $categoriesRepository->findAll();
			//$products = Products::findByCategory($_GET["ID"]);
			$products = $this->productsRepository->findByCategory($_GET["ID"]);
			require "Views/Products/searchByCategory.php";
		} else {
			header("Location: index.php?C=Products&A=promotions");
		}

	}

}