<?php

Class UsersController extends Controller {

	public function defaultAction(){

	}

	public function newAction(){
		require "Views/Users/new.php";
	}

	public function createAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$user = new Users();
			$user->firstname = $_POST["firstname"];
			$user->lastname = $_POST["lastname"];
			$user->email = $_POST["email"];
			$user->password = $_POST["password"];
			$status = Users::insert($user);
			require "Views/Users/create.php";
		} else {
			header("Location: index.php?C=Users&A=new");
		}
	}

	public function loginAction(){
		$errors = [];
		if(array_key_exists("users-connect-errors", $_SESSION)){
			$errors = $_SESSION["users-connect-errors"];
			unset($_SESSION["users-connect-errors"]);
		}
		require "Views/Users/login.php";
	}

	public function connectAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$validation = true;
			$errors = [];
			if(empty($_POST["email"])){
				$validation = false;
				array_push($errors,"Field 'email' is required.");
			} else if(!filter_var($_POST["email"],FILTER_VALIDATE_EMAIL)){
				$validation = false;
				array_push($errors,"Field 'email' has a wrong format.");
			}
			if(empty($_POST["password"])){
				$validation = false;
				array_push($errors,"Field 'password' is required.");
			}
			if($validation){
				$result = Users::checkCredentials($_POST["email"],$_POST["password"]);
				if($result){
					$_SESSION["user"] = serialize($result);
					setcookie("user",$result->id,time()+3600);
					if($result->administrator==1){
						setcookie("administrator",$result->id,time()+3600);
					}
					header("Location: index.php?C=Users&A=welcome");
				} else {
					$validation = false;
					array_push($errors,"User or password invalid.");
				}
			} 
			if(!$validation){
				$_SESSION["users-connect-errors"] = $errors;
				header("Location: index.php?C=Users&A=login");
			}
		} else {
			header("Location: index.php?C=Users&A=login");
		}
	}

	public function disconnectAction(){
		if(array_key_exists("user", $_COOKIE)){
			setcookie("user",NULL,time()-3600);
		} 
		if(array_key_exists("administrator", $_COOKIE)){
			setcookie("administrator",NULL,time()-3600);
		} 
		if(array_key_exists("user", $_SESSION)){
			unset($_SESSION["user"]);
		}
		header("Location: index.php?C=Users&A=login");
	}

	public function welcomeAction(){
		if(array_key_exists("user", $_COOKIE)){
			$user = unserialize($_SESSION["user"]);
			if(array_key_exists("administrator", $_COOKIE)){
				$administrator = true;
			} else {
				$administrator = false;
			}
			require "Views/Users/welcome.php";
		} else {
			header("Location: index.php?C=Users&A=login");
		}
	}

	public static function loginStatus(){
		if(array_key_exists("user", $_COOKIE)){
			$user = unserialize($_SESSION["user"]);
			require "Views/Users/loginStatusConnected.php";
		} else {
			require "Views/Users/loginStatusDisconnected.php";
		}



		
	}

}