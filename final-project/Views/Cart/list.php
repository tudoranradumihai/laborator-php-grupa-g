<h1>My Cart</h1>
<?php 
if(array_key_exists("cart", $_SESSION)){
?>
<table class="table">
  <thead>
    <tr>
      <th align="center" scope="col">#</th>
      <th align="center" scope="col">Product</th>
      <th align="center" scope="col">Unit Price</th>
      <th align="center" scope="col" colspan="2">Quantity</th>
      <th align="center" scope="col">Price</th>
    </tr>
  </thead>
<?php
$index = 1;
$total = 0;
	foreach($_SESSION["cart"] as $product => $quantity){
?>
<tr>
	<td align="center"><?php echo $index++; ?></td>
	<td><?php echo $products[$product]->getName(); ?></td>
	<td align="right"><?php echo number_format($products[$product]->getPrice(),2); ?> &euro;</td>
	<td align="center">
		<a href="index.php?C=Cart&A=update&ID=<?php echo $product ?>&QUANTITY=<?php echo $quantity-1; ?>"><input type="button" value="-"></a>
		<?php echo $quantity; ?> 
		<a href="index.php?C=Cart&A=update&ID=<?php echo $product ?>&QUANTITY=<?php echo $quantity+1; ?>"><input type="button" value="+"></a>
	</td>
	<td align="center">
		<a href="index.php?C=Cart&A=update&ID=<?php echo $product ?>&QUANTITY=0"><input type="button" value="Delete"></a>
	</td>
	
	<td align="right"><?php 
		$pricePerProduct = $quantity*$products[$product]->getPrice();
		$total += $pricePerProduct;
		echo number_format($pricePerProduct,2);
?> &euro;
	</td>
</tr>
<?php
	}
?>
	<tr>
		<td colspan="5" align="right">TOTAL</td>
		<td align="right"><?php echo number_format($total,2); ?> &euro;</td>
	</tr>
</table>
<?php
} else {
	echo "You have not added any products in your cart. Start Shopping now!";
}