<h1>My Shop Promotions</h1>
<b>Categories:</b>
<?php
foreach ($categories as $category) {
?>
<a href="index.php?C=Products&A=searchByCategory&ID=<?php echo $category->getId() ?>"><?php echo $category->getName() ?></a>
<?php	
}
?>



<div class="row">
<?php
foreach ($products as $product) {
?>
<div class="col-md-6">
	<a href="index.php?C=Products&A=show&ID=<?php echo $product->getId() ?>">
	<?php
	if($product->getImage()!=NULL){
	?>
	<img class="img-fluid" src="<?php echo $product->getImage() ?>">
	<?php } else { ?>
	NO IMAGE
	<?php } ?>
</a>	
	<h2><?php echo $product->getName()?></h2>
	<p><?php echo $product->getDescription()?></p>
	<p class="text-right"><?php echo number_format($product->getPrice(),2)?> &euro;</p>
</div>
<?php
}
?>
</div>