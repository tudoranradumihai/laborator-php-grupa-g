<h1><?php echo $product->getName() ?></h1>
<p><?php echo $product->getDescription() ?></p>
<p><strong><?php echo $product->getPrice() ?> &euro;</strong></p>

<form method="POST" action="index.php?C=Cart&A=addToCart">
	<input name="product" type="hidden" value="<?php echo $product->getId() ?>">
	<input name="quantity" type="number" min="1" max="10" value="1">
	<input type="submit" value="Add To Cart">
</form>