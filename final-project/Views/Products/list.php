<h1>List Products</h1>
<table class="table table-striped">
	<thead>
		<tr>
			<td>#</td>
			<td>Name</td>
			<td>Description</td>
			<td>Price</td>
			<td>Stock</td>
			<td colspan="3"></td>
		</tr>
	</thead>
	<?php foreach($products as $product) { ?>
		<tr>
			<td><?php echo $product->id ?></td>
			<td><?php echo $product->name ?></td>
			<td><?php echo $product->description ?></td>
			<td><?php echo $product->price ?> &euro;</td>
			<td><?php echo $product->stock ?></td>
			<td><a href="index.php?C=Products&A=show&ID=<?php echo $product->id ?>">Show</a></td>
			<td><a href="index.php?C=Products&A=edit&ID=<?php echo $product->id ?>">Edit</a></td>
			<td><a href="index.php?C=Products&A=delete&ID=<?php echo $product->id ?>">Delete</a></td>
		</tr>
	<?php } ?>
</table>
<a href="index.php?C=Products&A=new">New Product</a>