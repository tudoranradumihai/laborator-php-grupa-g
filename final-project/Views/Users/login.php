<h1>Login User</h1>
<?php 
  foreach ($errors as $error) {
    echo '<div class="alert alert-danger" role="alert">'.$error.'</div>';
  }
?>
<form id="userslogin" method="POST" action="index.php?C=Users&A=connect">
  <div class="form-group">
    <label for="email">Email Address</label>
    <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Enter email">
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>
  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
