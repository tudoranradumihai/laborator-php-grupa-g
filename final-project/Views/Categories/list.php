<h1>List Categories</h1>
<table class="table table-striped">
	<thead>
		<tr>
			<td>#</td>
			<td>Name</td>
			<td>Description</td>
			<td colspan="3"></td>
		</tr>
	</thead>
	<?php foreach($categories as $category) { ?>
		<tr>
			<td><?php echo $category->id ?></td>
			<td><?php echo $category->name ?></td>
			<td><?php echo $category->description ?></td>
			<td><a href="index.php?C=Categories&A=show&ID=<?php echo $category->id ?>">Show</a></td>
			<td><a href="index.php?C=Categories&A=edit&ID=<?php echo $category->id ?>">Edit</a></td>
			<td><a href="index.php?C=Categories&A=delete&ID=<?php echo $category->id ?>">Delete</a></td>
		</tr>
	<?php } ?>
</table>
<a href="index.php?C=Categories&A=new">New Category</a>