<?php

Abstract Class Repository {

	protected $database;
	protected $model;
	protected $table;

	public function __construct(){
		$this->database = new Database();
		$calledClass = get_called_class();
		$this->model = str_replace("Repository","",$calledClass);
		$this->table = strtolower($this->model);
	}

	public function insert($object){
		$properties = [];
		$values = [];
		foreach($object as $property => $value){
			if($value!=NULL){
				array_push($properties,$property);
				array_push($values,"'".$value."'");
			}
		}
		$properties = implode(",",$properties);
		$values = implode(",",$values);

		$query = "INSERT INTO $this->table ($properties) VALUES ($values);";
		$result = $this->database->query($query);
		return $result;
	}

	public function findAll(){
		$query = "SELECT * FROM $this->table";
		$result = $this->database->query($query);
		$array = [];
		while($line = mysqli_fetch_assoc($result)){
			$object = new $this->model();
			foreach($object->getProperties() as $property){
				$object->{"set".ucfirst($property)}($line[$property]);
			}
			array_push($array,$object);
		}
		return $array;
	}

	public function findByIds($ids){
		$temporary = [];
		foreach($ids as $id){
			$temporary[] = "id=$id";
 		}
		$query = "SELECT * FROM $this->table WHERE ".implode(" OR ",$temporary);	
		$result = $this->database->query($query);
		$array = [];
		while($line = mysqli_fetch_assoc($result)){
			$object = new $this->model();
			foreach($object->getProperties() as $property){
				$object->{"set".ucfirst($property)}($line[$property]);
			}
			$array[$line["id"]] = $object;
		}
		return $array;
	}

	public function findById($id){
		$query = "SELECT * FROM $this->table WHERE id=$id;";
		$result = $this->database->query($query);
		if(mysqli_num_rows($result)>0){
			$line = mysqli_fetch_assoc($result);
			$object = new $this->model();
			foreach($object->getProperties() as $property){
				$object->{"set".ucfirst($property)}($line[$property]);
			}
			return $object;
		} else {
			return NULL;
		}
	}

	public function update($object){
		$values = [];
		foreach($object as $property => $value){
			if($property!="id"){
				array_push($values, $property."='".$value."'");
			} else {
				$identifier = $value;
			}
		}
		$values = implode(", ",$values);
		$query = "UPDATE $this->table SET $values WHERE id=$identifier;";
		$result = $this->database->query($query);
		return $result;
	}

	public function delete($id){
		$query = "DELETE FROM $this->table WHERE id=$id;";
		$result = $this->database->query($query);
		return $result;
	}

	public function softDelete($id){
		$query = "UPDATE $this->table SET deleted=1 WHERE id=$id;";
		$result = $this->database->query($query);
		return $result;
	}

}