<?php

Class ProductsRepository extends Repository {

	public function delete($id){
		// HARD DELETE
		// $query = "DELETE FROM users WHERE id=$id;";
		
		// SOFT DELETE
		$query = "UPDATE products SET deleted=1 WHERE id=$id;";
		$result = $this->database->query($query);
	}

	public function findByCategory($category){
		$query = "SELECT * FROM products WHERE category=$category AND deleted=0;";
		$result = $this->database->query($query);
		$array = [];
		while($line = mysqli_fetch_assoc($result)){
			$object = new Products();
			foreach($object->getProperties() as $property){
				$object->{"set".ucfirst($property)}($line[$property]);
			}
			array_push($array,$object);
		}
		return $array;
	}

}