<?php

Class Categories extends Model {

	private $id;
	private $name;
	private $description;
	private $created;
	private $updated;

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getDeleted(){
		return $this->deleted;
	}

	public function setDeleted($deleted){
		$this->deleted = $deleted;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setDescription($description){
		$this->description = $description;
	}

	public function getCreated(){
		return $this->created;
	}
	public function setCreated($created){
		$this->created = $created;
	}
	public function getUpdated(){
		return $this->updated;
	}
	public function setUpdated($updated){
		$this->updated = $updated;
	}

}