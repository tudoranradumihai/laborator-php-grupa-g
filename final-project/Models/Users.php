<?php

Class Users {

	public $id;

	public $firstname;
	public $lastname;
	public $email;
	public $password;
	public $administrator;

	public $created;
	public $updated;

	public static function insert($user){
		$properties = [];
		$values = [];
		foreach($user as $property => $value){
			if($value!=NULL){
				array_push($properties,$property);
				array_push($values,"'".$value."'");
			}
		}
		$properties = implode(",",$properties);
		$values = implode(",",$values);

		$query = "INSERT INTO users ($properties) VALUES ($values);";
		$database = new Database();
		$result = $database->query($query);
		return $result;
	}

	public static function checkCredentials($email,$password){
		$query = "SELECT * FROM users WHERE email LIKE '$email' AND password LIKE '$password';";
		$database = new Database();
		$result = $database->query($query);
		if(mysqli_num_rows($result)>0){
			$line = mysqli_fetch_assoc($result);
			$user = new Users();
			foreach($user as $property => $value){
				$user->$property = $line[$property];
			}
			return $user;
		} else {
			return FALSE;
		}

	}

}