<?php

Abstract Class Model {

	public function getProperties(){
		$reflection = new ReflectionClass($this);
		$properties = $reflection->getProperties();
		$array = [];
		foreach($properties as $property){
			array_push($array,$property->name);
		}
		return $array;
	}

}