<?php

Class Products extends Model {

	private $id;
	private $deleted;
	private $name;
	private $description;
	private $price;
	private $stock;
	private $category;
	private $image;
	private $created;
	private $updated;

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getDeleted(){
		return $this->deleted;
	}

	public function setDeleted($deleted){
		$this->deleted = $deleted;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setDescription($description){
		$this->description = $description;
	}

	public function getPrice(){
		return $this->price;
	}

	public function setPrice($price){
		$this->price = $price;
	}

	public function getStock(){
		return $this->stock;
	}

	public function setStock($stock){
		$this->stock = $stock;
	}

	public function getCategory(){
		return $this->category;
	}

	public function setCategory($category){
		$this->category= $category;
	}

	public function getImage(){
		return $this->image;
	}
	
	public function setImage($image){
		$this->image= $image;
	}

	public function getCreated(){
		return $this->created;
	}
	public function setCreated($created){
		$this->created = $created;
	}
	public function getUpdated(){
		return $this->updated;
	}
	public function setUpdated($updated){
		$this->updated = $updated;
	}

}