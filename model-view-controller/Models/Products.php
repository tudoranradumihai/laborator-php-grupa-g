<?php

Class Products {

	public $id;
	public $name;
	public $description;

	public static function findAll(){
		$database = new Database();
		$query = "SELECT * FROM products";
		$result = $database->query($query);
		$array = [];
		while($line = mysqli_fetch_assoc($result)){
			$product = new Products();
			$product->id = $line["id"];
			$product->name = $line["name"];
			$product->description = $line["description"];
			array_push($array,$product);
		}
		return $array;
	}

}