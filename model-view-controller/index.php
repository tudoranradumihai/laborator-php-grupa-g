<?php
require "Helpers/Autoload.php";
?>
<!DOCTYPE html>
<html>
	<head>
	</head>
	<body>
		<?php require "Resources/Header.php"; ?>

		<?php
			// MVC Initialisation
			if (array_key_exists("C", $_GET)){
				$className = $_GET["C"]."Controller";
				if(class_exists($className)){
					$object = new $className();
					if(array_key_exists("A", $_GET)){
						$methodName = $_GET["A"]."Action";
						if(method_exists($object, $methodName)){
							$object->$methodName();
						} else {
							echo "ERROR: Method '$className::$methodName' doesn't exist.";
						}
					}
				} else {
					echo "ERROR: Class '$className' doesn't exist.";
				}
			}
		?>

		<?php require "Resources/Footer.php"; ?>
	</body>
</html>