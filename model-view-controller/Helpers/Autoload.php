<?php
// function __autoload is automatically called when a Class is not found. The class receives as parameter the name of the class.
function __autoload($class){
	// create a list of folder names
	$folders = ["Controllers","Models","Helpers"];
	// foreach folder, run the following script
	foreach ($folders as $folder){
		// create a filepath by using the folder name and the class name.
		$filepath = "$folder/$class.php";
		// verify if the created path leads to an existing file
		if (file_exists($filepath)){
			// include the following filepath
			require $filepath;
		}
	}

}