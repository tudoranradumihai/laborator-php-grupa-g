<?php
if($_SERVER["REQUEST_METHOD"]=="POST"){
	$file = fopen("archive.txt","r");
	$archive = fgets($file);
	fclose($file);
	if($archive!==false){
		$array = json_decode($archive,TRUE);
	} else {
		$array = array();
	}
	// selectare cheie si valoare
	$key = $_POST["fructe"];
	$value = $_POST["quantity"];
	// adaugare cantitate in array
	if(array_key_exists($key, $array)){
		$array[$key] += intval($value);
	} else {
		$array[$key] = intval($value);
	}
	$archive = json_encode($array);
	$file = fopen("archive.txt","w");
	fputs($file,$archive);
	fclose($file);
}
header("Location: add.php");