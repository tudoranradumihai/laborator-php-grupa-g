<?php
/* MODES
w  - deschide fisierul (daca nu exista il creaza), il truncheaza si pune pointerul de scriere la inceputul fisierului.
a+ - deschide fisierul (daca nu exista il creaza), nu il truncheaza ci pune pointerul la sfarsitul fisierului.

*/
$file = fopen("monkey.txt","w");
$content = "Pan troglodytes";
fputs($file,$content.PHP_EOL);
fclose($file);