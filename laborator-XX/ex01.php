<?php

$string = "Consiliul UE a adoptat vineri o decizie prin care a stabilit că România nu a reuşit  să ia măsuri eficiente pentru a corecta o abatere bugetară semnificativă. Recomandarea trimisă României în cadrul „procedurii de abatere semnificativă” este cea de-a treia începând cu luna iunie 2017.   „România a fost supusă unei proceduri de abatere semnificativă din iunie 2017, după o deteriorare a soldului său structural în 2016 şi o deteriorare ulterioară în 2017. În decembrie 2017, Consiliul a stabilit că România nu a luat măsuri eficiente”, a informat Consiliul UE.    Oficialii europeni au cerut României să ia măsuri urgente pentru a se asigura că majorarea nominală a cheltuielilor publice nete primare nu depăşeşte 3,3% din PIB în 2018, reprezentând o ajustare structurală anuală de cel puţin 0,8% din PIB.   Consiliulul UE arată că soldul structural al României s-a deteriorat la -3,3% din PIB, iar cheltuielile publice nete primare au fost cu mult peste nivelul de referinţă stabilit de Consiliu. „Ambii indicatori confirmă o abatere bugetară semnificativă. Iar nerespectarea recomandărilor anterioare - cu riscul depăşirii valorii de referinţă de 3% din PIB a UE pentru deficitele publice - necesită măsuri urgente”, se arată în informarea Consiliului. Această a treia recomandare trebuie aplicată prin măsuri care trebuie întreprinse atât în 2018, cât şi în 2019.   României i se recomandă să ia măsuri pentru a se asigura că creşterea nominală a cheltuielilor publice nete primare nu va depăşi 3,3% în 2018 şi 5,1% în 2019, reprezentând o ajustare structurală anuală de 0,8% din PIB în ambii ani.   „Eventualele câştiguri neaşteptate ar trebui utilizate pentru reducerea deficitului, iar măsurile de consolidare bugetară ar trebui să asigure o îmbunătăţire durabilă. Consiliul a stabilit un termen limită pentru 15 octombrie 2018 pentru ca România să raporteze cu privire la măsurile întreprinse”, se arată în documentul Consiliului UE.";
$content = [".",",","0","1","2","3","4","5","6","7","8","9",'"',"'","-","%","„","”"];
$string = str_replace($content,"",$string);
$string = strtolower($string);
$string = explode(" ",$string);

$array = [];

foreach($string as $word){
	echo strlen($word)."<br>";
	if(array_key_exists(strlen($word), $array)){
		$array[strlen($word)]++;
	} else {
		$array[strlen($word)] = 1;
	}
}




