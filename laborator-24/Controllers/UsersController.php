<?php

Class UsersController {

	public function listAction(){
		$users = Users::findAll();
		include "Views/Users/list.php";
	}

	public function showAction(){
		if(array_key_exists("ID", $_GET)){
			$user = Users::findByID($_GET["ID"]);
			include "Views/Users/show.php";
		} else {
			header("Location: index.php?C=Users&A=list");
		}
	}

	public function newAction(){
		include "Views/Users/new.php";
	}

	public function createAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$user = new Users();
			$user->firstname = $_POST["firstname"];
			$user->lastname = $_POST["lastname"];
			$user->email = $_POST["email"];
			Users::insert($user);
			header("Location: index.php?C=Users&A=list");
		} else {
			header("Location: index.php?C=Users&A=new");
		}
	}

	public function editAction(){
		if(array_key_exists("ID", $_GET)){
			$user = Users::findByID($_GET["ID"]);
			include "Views/Users/edit.php";
		} else {
			header("Location: index.php?C=Users&A=list");
		}
	}

	public function updateAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$user = new Users();
			$user->id = $_POST["id"];
			$user->firstname = $_POST["firstname"];
			$user->lastname = $_POST["lastname"];
			$user->email = $_POST["email"];
			Users::update($user);
		}
		header("Location: index.php?C=Users&A=list");
	}

	public function deleteAction(){
		if(array_key_exists("ID", $_GET)){
			Users::delete($_GET["ID"]);
		}
		header("Location: index.php?C=Users&A=list");
	}

}