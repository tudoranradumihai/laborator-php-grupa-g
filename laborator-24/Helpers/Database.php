<?php

Class Database {

	private $connection;

	public function __construct(){
		$this->connection = mysqli_connect(
			"localhost",
			"root",
			"",
			"MVC-01"
		);
	}

	public function query($query){
		return mysqli_query($this->connection,$query);
	}

	public function __destruct(){
		mysqli_close($this->connection);
	}
}