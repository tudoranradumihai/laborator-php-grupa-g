<?php
	require "Helpers/Autoload.php";
?>
<a href="index.php">Homepage</a>
<a href="index.php?C=Users&A=list">List Users</a>
<a href="index.php?C=Users&A=new">New User</a>
<?php
/*
$object = new UsersController();
$object->listAction();
*/
if(array_key_exists("C", $_GET)){
	$controller = $_GET["C"]."Controller";
	if(class_exists($controller)){
		$object = new $controller();
		if(array_key_exists("A", $_GET)){
			$action = $_GET["A"]."Action";
			if(method_exists($object, $action)){
				$object->$action();
			} else {
				echo "<b>ERROR:</b> Method '$controller::$action' doesn't exist.";
			}
		}
	} else {
		echo "<b>ERROR:</b> Class '$controller' doesn't exist.<br>";
	}
}