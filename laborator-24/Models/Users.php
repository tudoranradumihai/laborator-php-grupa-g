<?php

Class Users {

	public $id;
	public $firstname;
	public $lastname;
	public $email;

	public static function insert($user){
		/*
		$query = "INSERT INTO users (firstname,lastname,email) VALUES ('$user->firstname','$user->lastname','$user->email');";
		*/
		$properties = [];
		$values = [];
		foreach($user as $property => $value){
			if($value!=NULL){
				array_push($properties,$property);
				array_push($values,"'".$value."'");
			}
		}
		$properties = implode(",",$properties);
		$values = implode(",",$values);

		$query = "INSERT INTO users ($properties) VALUES ($values);";
		$database = new Database();
		$database->query($query);
		
	}

	public static function update($user){
		$values = [];
		foreach($user as $property => $value){
			if($property!="id"){
				$temporary = "$property='$value'";
				array_push($values,$temporary);
			}
		}
		$values = implode(", ",$values);
		$query = "UPDATE users SET $values WHERE id=$user->id;";
		$database = new Database();
		$database->query($query);
	}

	public static function findAll(){
		$query = "SELECT * FROM users";
		$database = new Database();
		$result = $database->query($query);
		$array = [];
		while($line = mysqli_fetch_assoc($result)){
			$object = new Users();
			foreach($object as $property => $value){
				$object->$property = $line[$property];
			}
			array_push($array,$object);
		}
		return $array;
	}

	public static function findByID($id){
		$query = "SELECT * FROM users WHERE id=$id;"; 
		$database = new Database();
		$result = $database->query($query);
		$line = mysqli_fetch_assoc($result);
		$object = new Users();
		foreach($object as $property => $value){
			$object->$property = $line[$property];
		}
		return $object;
	}

	public static function delete($id){
		$query = "DELETE FROM users WHERE id=$id;";
		$database = new Database();
		$result = $database->query($query);
	}

}