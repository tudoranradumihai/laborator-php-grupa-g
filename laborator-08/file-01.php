<?php

$string = "text";

//echo $string;

// array neasociativ cu valori numerice
$array = array(1,2,3,4,5);
$array = json_encode($array);
echo $array."<br>";

// array asociativ cu valori numerice
$array = array(1,2,3,4,5);
$array[10] = 100;
$array = json_encode($array);
echo $array."<br>";

// array neasociativ cu valori text
$array = array("unu","doi");
$array = json_encode($array);
echo $array."<br>";

// array asociativ cu valori text
$array = array("cheie1"=>"unu","cheie2"=>"doi");
$array = json_encode($array);
echo $array."<br>";