<?php

/*
Sa se genereze un array "currencies" care contine toate numele de monede care au intre 0 si 1 EUR
*/

// 3187fc9c3233ae39bf310fcf79956e09
$link = "http://data.fixer.io/api/latest?access_key=3187fc9c3233ae39bf310fcf79956e09";
$content = file_get_contents($link);
$array = json_decode($content,TRUE);

$currencies = array();
foreach($array["rates"] as $key => $value){
	if($value>0 && $value<1){
		array_push($currencies,$key);
	}
}
var_dump($currencies);
