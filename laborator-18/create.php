<?php
session_start();
if($_SERVER["REQUEST_METHOD"]=="POST"){
	$validation = true;
	$errors = array(); 
	if(empty($_POST["name"])){
		$validation = false;
		$errors[] = "Name required.";
	}
	if(empty($_POST["email"])){
		$validation = false;
		$errors[] = "email required.";
	} else if (!filter_var($_POST["email"],FILTER_VALIDATE_EMAIL)){
		$validation = false;
		$errors[] = "wrong email format.";
	}

	if($validation){
		$connection = mysqli_connect("localhost","root","","laborator-18");
		$query = "INSERT INTO users (name,email) VALUES ('$_POST[name]','$_POST[email]');";
		$result = mysqli_query($connection,$query);
		mysqli_close($connection);
		header("Location: index.php");
	} else {
		$_SESSION["errors"] = $errors;
		header("Location: new.php");
	}


} else {
	header("Location: new.php");
}