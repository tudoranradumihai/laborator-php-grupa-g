CREATE DATABASE mydatabase;


/* Creare tabel */
CREATE TABLE users(
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	firstname VARCHAR(255) NOT NULL,
	lastname VARCHAR(255) NOT NULL,
	birthdate DATE,
	gender INT(11),
	address VARCHAR(255),
	city VARCHAR(255),
	country VARCHAR(255),
	email VARCHAR(255) UNIQUE  NOT NULL,
	password VARCHAR(255) NOT NULL
);

/* stergere tabel */
DROP TABLE users;

/* ADAUGARE DATE IN TABEL */
/*
STRUCTURE
insert into table_name (col1,col2,...)
 */
INSERT INTO users (firstname,lastname,birthdate,email,password) 
VALUES ('John','Doe','1987-01-01','john@ceva.com','1234');

/* select all from table */
SELECT * FROM users;
SELECT firstname,lastname,email FROM users;
SELECT * FROM users WHERE id=1;
SELECT * FROM users WHERE firstname LIKE 'John'; 
SELECT * FROM users WHERE firstname LIKE 'J%';
SELECT * FROM users WHERE email LIKE '%gmail.com';
SELECT * FROM users WHERE email LIKE '%gmail.com' AND firstname LIKE 'J%';

SELECT * FROM users /* WHERE ... */ ORDER BY lastname ASC;
SELECT * FROM users /* WHERE ... ORDER BY ... */ LIMIT 10;
SELECT * FROM users /* WHERE ... ORDER BY ... */ LIMIT 10 OFFSET 10;

UPDATE users SET firstname='Radu',lastname="Tudoran" WHERE id=3;

DELETE FROM users WHERE id=4;