<?php
// http://php.net/manual/en/ref.strings.php
$a = "AbEceDaRRR";

echo $a."<br>";
// http://php.net/manual/en/function.strtoupper.php
echo strtoupper($a)."<br>";
// http://php.net/manual/en/function.strtolower.php
echo strtolower($a)."<br>";
// http://php.net/manual/en/function.ucfirst.php
echo ucfirst("ana are mere")."<br>";
// http://php.net/manual/en/function.lcfirst.php
echo lcfirst($a)."<br>";
// http://php.net/manual/en/function.ucwords.php
echo ucwords("ana are mere")."<br>";
// http://php.net/manual/en/function.strlen.php
echo strlen($a)."<br>";
// http://php.net/manual/en/function.strpos.php
var_dump(strpos("ana are mere","m"));
echo "<br>";
//http://php.net/manual/en/function.str-replace.php
echo str_replace("mere","pere","ana are mere")."<br>";