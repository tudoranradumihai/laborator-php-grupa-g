<!-- http://getbootstrap.com/ -->
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<h1>My First Form</h1>
			<form action="confirmation.php" method="POST">
				<div class="form-group">
					<label for="name">Name:</label>
					<input type="text" class="form-control" id="name" placeholder="John Doe" name="name">
				</div>
				<div class="form-group">
					<label for="email">Email Address:</label>
					<input type="text" class="form-control" id="email" placeholder="john.doe@domain.com" name="email">
				</div>
				<div class="form-group">
					<label for="password">Password:</label>
					<input type="password" class="form-control" id="password" placeholder="" name="password">
				</div>
				<div class="form-group">
					<label for="password2">Confirm Password:</label>
					<input type="password" class="form-control" id="password2" placeholder="" name="password2">
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</body>
</html>

<!-- OLD FILE -->
<!--
<form action="confirmation.php" method="POST">
	<div>
		<label for="name">Name :</label><br>
		<input id="name" type="text" name="name">
	</div>
	<div>
		<label for="email">Email Address :</label><br>
		<input id="email" type="text" name="email">
	</div>
	<div>
		<label for="password">Password :</label><br>
		<input id="password" type="password" name="password">
	</div>
	<div>
		<label for="password2">Confirm Password :</label><br>
		<input id="password2" type="password" name="password2">
	</div>
	<input type="submit">
</form>
-->