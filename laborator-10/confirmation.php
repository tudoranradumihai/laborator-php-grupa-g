<?php

if($_SERVER["REQUEST_METHOD"]=="POST"){

	$validation = true;
	$errors = array();

	if(empty($_POST["name"])){
		array_push($errors, "Field 'name' is required.");
		$validation = false;
	}

	if(empty($_POST["email"])){
		array_push($errors, "Field 'email' is required.");
		$validation = false;
	} else if(!filter_var($_POST["email"],FILTER_VALIDATE_EMAIL)){
		array_push($errors, "Field 'email' invalid format.");
		$validation = false;
	}

	if(empty($_POST["password"])){
		array_push($errors, "Password is required.");
		$validation = false;
	} else if(strlen($_POST["password"])<8){
		array_push($errors, "Password must have at least 8 characters.");
		$validation = false;
	} else if (empty($_POST["password2"])){
		array_push($errors, "Confirm Password is required.");
		$validation = false;
	} else if ($_POST["password"]!=$_POST["password2"]){
		array_push($errors, "Passwords don't match.");
		$validation = false;
	}

} else {
	header("Location: homework.php");
}
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<h1>My First Form</h1>
		<?php
			if($validation==true){
				echo '<div class="alert alert-success" role="alert">OK</div>';
			} else {
				foreach($errors as $error){
					echo '<div class="alert alert-danger" role="alert">'.$error.'</div>';
				}
			}
		?>
		</div>
	</body>
</html>







