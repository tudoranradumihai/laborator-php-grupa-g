<?php
// EX 01
/*
Sa se afiseze random numele unui cursant folosind functia rand.
Se poate utiliza array,count,rand,echo
*/

// VARIANTA BEGGINER
$array = array("Cristina","Cecilia","Claudia","Cristiana","Cora","Ana");
$numarDeElemente = count($array);
$cheieMaxima = $numarDeElemente-1;
$randomNumber = rand(0,$cheieMaxima);
echo $array[$randomNumber];

// VARIANTA PRO
$array = array("Cristina","Cecilia","Claudia","Cristiana","Cora","Ana");
echo $array[rand(0,count($array)-1)];