1. Variabile (descriere,tip) (file-01.php)
2. Functii matematice (file-02.php)
3. Functii cu siruri de caractere (file-03.php)
4. Array-uri, functii cu array-uri (file-04.php)

HOMEWORK
1. sa se genereze un array ce contine 10 numere pare de 6 cifre divizibile cu 13 care contin cifra 4
2. sa se genereze un cuvant intre 3 si 12 litere care incepe cu litera a sau e. Urmatoarea litera trebuie sa fie consoana. Pe urma literele vor alterna aleator insa avem posibilitatea de a avea 2 vocale una langa alta.
3. sa se genereze un script care returneaza aleator o reteta. reteta va contine 2 fructe si 3 legume.
fructele si legumele sunt 2 liste sparate de minim 10 elemente. 