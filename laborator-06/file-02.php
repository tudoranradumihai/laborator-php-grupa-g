<?php

// FUNCTII MATEMATICE
// http://php.net/manual/en/ref.math.php

// http://php.net/manual/en/function.round.php
echo round(1.4950)."<br>"; // 1
echo round(1.51)."<br>"; // 1
echo round(1.3550,2)."<br>"; 
echo round(1.5,0,PHP_ROUND_HALF_DOWN)."<br>"; 
echo round(1.6,0,PHP_ROUND_HALF_DOWN)."<br>"; 

// http://php.net/manual/en/function.ceil.php
echo ceil(1.1)."<br>";

// http://php.net/manual/en/function.floor.php
echo floor(1.7)."<br>";

// http://php.net/manual/en/function.abs.php
echo abs(-5)."<br>";	

// http://php.net/manual/en/function.rand.php
echo rand(100,999);
