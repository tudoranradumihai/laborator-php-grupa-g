<?php
/* 3.1
Sa se genereze un string de 5 caractere folosind toate literele common din alfabet.
*/
$alfabet = "abcdefghijklmnoprstuvxyz";
$contor = 0;
$cuvant = "";
while($contor<5){
	$cuvant .= $alfabet[rand(0,strlen($alfabet)-1)];
	$contor++;
}
echo $cuvant."<br>";

/* 3.2
Sa se genereze un string de x caractere folosind toate literele common din alfabet. X fiind intre 3 si 8.
*/

$alfabet = "abcdefghijklmnoprstuvxyz";
$contor = 0;
$cuvant = "";
while($contor<rand(3,8)){
	$cuvant .= $alfabet[rand(0,strlen($alfabet)-1)];
	$contor++;
}
echo $cuvant."<br>";

/* 3.2
Sa se genereze un string de x caractere folosind toate literele common din alfabet. X fiind intre 3 si 8. Cuvantul trebuie sa inceapa cu o vocala si trebuie sa fie urmat de o consoana. Vocalele si consoanele vor alterna.
*/




$consoane = "bcdfghjklmnprstvwxyz";
$vocale = "aeiou";
$contor = 1;
$cuvant = "";
while($contor<=rand(3,8)){
	if($contor%2==0){
		$cuvant .= $consoane[rand(0,strlen($consoane)-1)];
	} else {
		$cuvant .= $vocale[rand(0,strlen($vocale)-1)];
	}
	$contor++;
}
echo $cuvant."<br>";


$consoane = "bcdfghjklmnprstvwxyz";
$vocale = "aeiou";

$numarPropozitii = rand(15,30);
$contorPropozitii = 0;
$propozitii = array();
while($contorPropozitii<$numarPropozitii){
	$numarCuvinte = rand(5,12);
	$contorCuvinte = 0;
	$cuvinte = array();
	while($contorCuvinte<$numarCuvinte){
		$contor = 1;
		$cuvant = "";
		while($contor<=rand(3,8)){
			if($contor%2==0){
				$cuvant .= $consoane[rand(0,strlen($consoane)-1)];
			} else {
				$cuvant .= $vocale[rand(0,strlen($vocale)-1)];
			}
			$contor++;
		}
		$cuvinte[] = $cuvant;
		$contorCuvinte++;
	}
	$propozitii[] = ucfirst(implode(" ",$cuvinte)).".";
	$contorPropozitii++;
}

echo implode(" ",$propozitii);