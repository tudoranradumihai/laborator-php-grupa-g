<?php

function generateWord(){
	$consoane = "bcdfghjklmnprstvwxyz";
	$vocale = "aeiou";
	$contor = 1;
	$cuvant = "";
	while($contor<=rand(3,8)){
		if($contor%2==0){
			$cuvant .= $consoane[rand(0,strlen($consoane)-1)];
		} else {
			$cuvant .= $vocale[rand(0,strlen($vocale)-1)];
		}
		$contor++;
	}
	return $cuvant;
}

function generatePhrase(){
	$contor = 1;
	$numarCuvinte = rand(5,12);
	$cuvinte = array();
	while($contor<$numarCuvinte){
		$cuvinte[] = generateWord();
	}
	return ucfirst(implode(" ",$cuvinte)).".";
}

generatePhrase();