<?php

// DECLARARE
$a = true; // variabila boolean
$b = false; // variabila boolean
$c = 1; // variabila integer
$d = 1.5; // variabila float
$e = "TEXT"; // variabila string

// MODIFICARE TIP 

$f = 1;
var_dump($f);
// modificare int > float
$g = (float)$f; // 1
var_dump($g);
// http://php.net/manual/en/function.floatval.php
$h = floatval($f); // 2 
var_dump($h);
// 
settype($f, "float"); // 3
var_dump($f);

// STRINGURI
$text = "Lorem ipsum dolor sit amet.";
echo $text;
$a1 = "text1";
$a2 = "text2";
echo $a1." ".$a2;
echo $a1."<br>".$a2;

$nume = "Stropinela";

echo "Buna ".$nume."!<br>";
echo "Buna $nume!<br>"; // analizeaza variabile
echo 'Buna $nume!<br>'; // nu analizeaza variabile
echo "Buna \$nume!<br>"; // escape string \