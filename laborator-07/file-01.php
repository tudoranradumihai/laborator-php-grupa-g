<?php
// http://php.net/manual/en/ref.array.php


// http://php.net/manual/en/function.is-array.php
$a1 = 1;
if(is_array($a1)){
	echo "a1 este array<br>";
} else {
	echo "a1 nu este array<br>";
}

$a2 = array(1,2);
if(is_array($a2)){
	echo "a2 este array<br>";
} else {
	echo "a2 nu este array<br>";
}

// http://php.net/manual/en/function.array-values.php
$a3 = array(1,2,3,4,5);
var_dump($a3); echo "<br>";
unset($a3[3]);
var_dump($a3); echo "<br>";
$a3 = array_values($a3);
var_dump($a3); echo "<br>";

$a4 = array("firstname"=>"John","lastname"=>"Doe");
var_dump($a4); echo "<br>";
$a4 = array_values($a4);
var_dump($a4); echo "<br>";

// http://php.net/manual/en/function.array-keys.php
$a5 = array("firstname"=>"John","lastname"=>"Doe");
var_dump($a5); echo "<br>";
$a5 = array_keys($a5);
var_dump($a5); echo "<br>";

// http://php.net/manual/en/function.array-combine.php
$a61 = array("k1","k2");
$a62 = array("v1","v2");
$a6 = array_combine($a61,$a62);
var_dump($a6); echo "<br>";

// http://php.net/manual/en/function.array-intersect.php
// http://php.net/manual/en/function.array-diff.php

// http://php.net/manual/en/function.array-merge.php
$a71 = array(1,3);
$a72 = array(1,2,4);
$a7 = array_merge($a71,$a72);
var_dump($a7); echo "<br>";

// http://php.net/manual/en/function.array-key-exists.php
$a8 = array("k1"=>"v1","k2"=>"v2");
if(array_key_exists("k2", $a8)){
	echo "cheia k2 exista in a8<br>";
} else {
	echo "cheia k2 nu exista in a8<br>";
}

// http://php.net/manual/en/function.in-array.php
if(in_array("v2", $a8)){
	echo "valoarea v2 exista in a8<br>";
} else {
	echo "valoarea v2 nu exista in a8<br>";
}

// http://php.net/manual/en/function.array-push.php
$a9 = array(1,2,3);
$a9[] = 4;
array_push($a9,5);

// http://php.net/manual/en/function.array-pop.php
$a9 = array(1,2,3);
echo array_pop($a9); echo "<br>";
var_dump($a9); echo "<br>";

// http://php.net/manual/en/function.array-rand.php
$a10 = array(1,2,3,4,5);
var_dump(array_rand($a10)); echo "<br>";
var_dump(array_rand($a10,2)); echo "<br>";

// http://php.net/manual/en/function.sort.php
$a11 = array(3,2,1,5,4);
sort($a11); 
var_dump($a11); echo "<br>";
$a12 = array("a","c","b");
sort($a12);
var_dump($a12); echo "<br>";	