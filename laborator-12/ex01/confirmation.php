<?php
session_start();

if($_SERVER["REQUEST_METHOD"]=="POST"){

	$validation = true;
	$errors = array();
	$data = array();

	if(empty($_POST["name"])){
		$validation = false;
		$errors[] = "Field 'name' is required.";
	} else {
		$data["name"] = $_POST["name"];
	}

	if(!array_key_exists("gender", $_POST)){
		$validation = false;
		$errors[] = "Field 'gender' is required.";
	} else {
		$data["gender"] = $_POST["gender"];
	}

	if(empty($_POST["email"])){
		$validation = false;
		$errors[] = "Field 'email' is required.";
	} else if (!filter_var($_POST["email"],FILTER_VALIDATE_EMAIL)){
		$validation = false;
		$errors[] = "Wrong email format.";
	} else {
		$data["email"] = $_POST["email"];
	}

	if($validation){
		var_dump($_POST);
	} else {
		$_SESSION["errors"] = $errors;
		$_SESSION["data"]	= $data;
		header("Location: index.php");
	}

} else {
	header("Location: index.php");
}