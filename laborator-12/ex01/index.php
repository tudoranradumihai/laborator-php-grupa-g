<?php
session_start();

if(array_key_exists("errors", $_SESSION)){
	foreach($_SESSION["errors"] as $error){
		echo $error."<br>";
	}
	unset($_SESSION["errors"]);
}

function checkField($field){
	if(array_key_exists("data", $_SESSION)) { 
		if (array_key_exists($field, $_SESSION["data"])) { 
			echo $_SESSION["data"][$field]; 
		} 
	}
}

function checkRadio($field,$value){
	if(array_key_exists("data", $_SESSION)){
		if(array_key_exists($field, $_SESSION["data"])){
			if($_SESSION["data"][$field]==$value){
				echo "checked";
			}
		}
	}
}
?>

<form method="POST" action="confirmation.php">
	<input type="text" name="name" placeholder="Name" value="<?php checkField("name"); ?>" ><br>

	<input type="radio" id="gender1" name="gender" <?php checkRadio("gender","1"); ?> value="1"> 
	<label for="gender1">Male</label><br>
	<input type="radio" id="gender2" name="gender" <?php checkRadio("gender","2"); ?> value="2"> 
	<label for="gender2">Female</label><br>


	<input type="text" name="email" placeholder="email@domain.com" value="<?php checkField("email"); ?>"><br>
	<input type="submit">
</form>
<?php
if(array_key_exists("data", $_SESSION)){
	unset($_SESSION["data"]);
}
?>