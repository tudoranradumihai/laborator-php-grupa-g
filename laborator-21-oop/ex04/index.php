<!DOCTYPE html>
<html>
	<head>
		<?php include "Resources/HeaderResources.php"; ?>
	</head>
	<body>
		<?php include "Resources/Header.php"; ?>
		<?php 
		if(array_key_exists("page", $_GET)){
			$path = "Pages/".ucfirst($_GET["page"]).".php";
		} else {
			$path = "Pages/Home.php";
		}
		if(file_exists($path)){
			include $path;
		} else {
			echo "<div>404</div>";
		} ?>
		<?php include "Resources/Footer.php"; ?>
		<?php include "Resources/FooterResources.php"; ?>
	</body>
</html>