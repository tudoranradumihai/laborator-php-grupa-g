<?php

// METODA PROCEDURALA
$query = "SELECT * FROM users";
$connection = mysqli_connect("localhost","root","","database");
$result = mysqli_query($connection,$query);
mysqli_close($connection);

// METODA OBIECTUALA

Class Database {

	private $connection;

	public function __construct(){
		$this->connection = mysqli_connect("localhost","root","","database");
	}

	public function query($query){
		return mysqli_query($this->connection,$query);
	}

	public function __destruct(){
		mysqli_close($this->connection);
	}
}

$database = new Database();
$result = $database->query("SELECT * FROM users");