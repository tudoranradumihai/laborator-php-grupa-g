<?php

function __autoload($class){
	$folders = array("Controllers","Models");
	foreach($folders as $folder){
		$path = $folder."/".$class.".php";
		if(file_exists($path)){
			require $path;
		}
	}
}

$object1 = new C1();

$object3 = new C3();