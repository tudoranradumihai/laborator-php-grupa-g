<?php

Class Car {
	public $mark;
	public $model;

	public function __construct($mark,$model){
		$this->mark = $mark;
		$this->model = $model;
	}
}

$object = new Car("BMW","320D");
