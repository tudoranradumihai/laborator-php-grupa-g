<?php
/*
index.php represents the Front Controller of the MVC Application
---
The front controller pattern is where you have a single entrance point for your web application (e.g. index.php) that handles all of the requests.
*/


/*
__autoload - represents a magic function that is automatically called when a called class is not found. The __autoload will try to include the file to find the class.
*/
function __autoload($class){
	$file = "Controllers/$class.php";
	if(file_exists($file)){
		include $file;
	}
}
?>
<a href="index.php">Homepage</a>
<a href="index.php?C=Products&A=list">List Products</a>
<?php
/*
$object = new ProductsController();
$object->listAction();
*/
if(array_key_exists("C", $_GET)){
	$controller = $_GET["C"]."Controller";
	if(class_exists($controller)){
		$object = new $controller();
		if(array_key_exists("A", $_GET)){
			$action = $_GET["A"]."Action";
			if(method_exists($object, $action)){
				$object->$action();
			}
		}
	} else {
		echo "ERROR: Class '$controller' doesn't exist.";
	}
}
